package pdf.template;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.google.common.io.Files;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.HorizontalAlignment;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.TableTest;
import be.quodlibet.boxable.line.LineStyle;
import be.quodlibet.boxable.utils.ImageUtils;

public class Factura1 {

public static void main(String[] args) throws IOException {

		
		// Initialize Document
		PDDocument doc = new PDDocument();
		PDPage page = new PDPage(PDRectangle.A4);
		doc.addPage(page);
		
		// Set margins
		float margin = 20;
		
		// Initialize first table and some values for the other ones.
		float yStartNewPage = page.getMediaBox().getHeight() - (margin);
		float tableWidth = page.getMediaBox().getWidth() - (2 * margin);
		boolean drawContent = true;
		float yStart = yStartNewPage;
		float bottomMargin = margin;
		
	// Create first table - Header Table	
		BaseTable invoiceHeaderTable = new BaseTable(yStart, yStartNewPage, bottomMargin, tableWidth, margin, 
				doc, page, false, drawContent);

	// Create the first row with three cells, containing the names for company and client, written with different font
		Row<PDPage> firstRow = invoiceHeaderTable.createRow(10f);
		generateFirstRowHeaderTable(firstRow);
		
	// Create the second row with three cells, with data for company, invoice and client
		Row<PDPage> secondRow = invoiceHeaderTable.createRow(80f);
		generateSecondRowHeaderTable(secondRow);
		
		
		
		
	//Create second table - PRODUCTS TABEL
		PDPage pageSecondTable = invoiceHeaderTable.getCurrentPage();
		float yStartNewPageSecondTabel = 820;
		float yStartSecondTabel = invoiceHeaderTable.draw();
		
		BaseTable productsTabel = new BaseTable(yStartSecondTabel, yStartNewPageSecondTabel, bottomMargin, tableWidth, margin, 
				doc, pageSecondTable, true, true);

	// Create Header row
		Row<PDPage> headerRow = productsTabel.createRow(15f);
		generateHeaderRowProductsTabel(headerRow);
		
		productsTabel.addHeaderRow(headerRow);
		
	//Create number row	
		Row<PDPage> numberRow = productsTabel.createRow(12f);
		generateNumberRow(numberRow, new String[] {"0", "1", "2", "3", "4", "5", "6", "7"});
		
	//Create some rows to populate the table
		for (int i = 0; i < 20; i++) {
			Row<PDPage> productRow1 = productsTabel.createRow(12f);
			generateProductRow(productRow1, new String[] {Integer.toString((3*i) + 1), "Monitor Samsung 24 inch HDMI", "buc", "1", "19%", "456", "456", "75"});
			
			Row<PDPage> productRow2 = productsTabel.createRow(12f);
			generateProductRow(productRow2, new String[] {Integer.toString((3*i) + 2), "Laptop Lenovo, 4 GB RAM, HDD 2 T, HDMI, bluetooth, display 17 inch, cd-rom, video nVidia 4 Gb, 5 usb 5.0, 4GB, 64GB eMMC, Intel HD Graphics 500, Microsoft Windows 10 S, Black", "buc", "1", "19%", "4506", "4506", "275"});
			
			Row<PDPage> productRow3 = productsTabel.createRow(12f);
			generateProductRow(productRow3, new String[] {Integer.toString((3*i) + 3), "Telefon mobil Apple iPhone 6, 32GB, Space Gray", "buc", "1", "19%", "3598", "3598", "225"});
		}
		
	//Create Empty Rows		
		for (int i = 0; i < 15; i++) {
			Row<PDPage> emptyRow1 = productsTabel.createRow(12f);
			generateEmptyRow(emptyRow1, new String[] {"", "", "", "", "", "", "", ""});
		}

		
		
	//Create footer table - FOOTER TABLE	
		float startY = productsTabel.draw();
		PDPage p = productsTabel.getCurrentPage();
		float startNewPageY1 =  820;

		BaseTable footerTable = new BaseTable(startY, startNewPageY1, margin, tableWidth, margin, doc,
				p, true, true);
			
	//Create SubTotal Row
		Row<PDPage> subTotalRow = footerTable.createRow(12f);
		Cell<PDPage> cellSubTotal = subTotalRow.createCell(80, "Total ");
		cellSubTotal.setAlign(HorizontalAlignment.RIGHT);
		
		Cell<PDPage> cellSubTotalValoare = subTotalRow.createCell(10, "256488");
		cellSubTotalValoare.setAlign(HorizontalAlignment.RIGHT);
		
		Cell<PDPage> cellSubTotalValoareTVA = subTotalRow.createCell(10, "1648");
		cellSubTotalValoareTVA.setAlign(HorizontalAlignment.RIGHT);
			
	//Create Total Row
		Row<PDPage> totalRow = footerTable.createRow(12f);
		Cell<PDPage> cellTotal = totalRow.createCell(90, "<b>Total de plata</b> (col. 6 + col. 7) ");
		cellTotal.setAlign(HorizontalAlignment.RIGHT);
		
		Cell<PDPage> cellTotalValoare = totalRow.createCell(10, "7356488");
		cellTotalValoare.setAlign(HorizontalAlignment.RIGHT);
		
	//Create Termen de plata Row
		Row<PDPage> termenRow = footerTable.createRow(12f);
		Cell<PDPage> cellTermen = termenRow.createCell(90, "<b>Termen de plata</b> (zi/luna/an) ");
		cellTermen.setAlign(HorizontalAlignment.RIGHT);
		
		Cell<PDPage> celltermenData = termenRow.createCell(10, "05/03/2018");
		celltermenData.setAlign(HorizontalAlignment.RIGHT);	
			
	// Create Semnatura and Date privind expeditia row
		Row<PDPage> expeditieRow = footerTable.createRow(55f);
		Cell<PDPage> cellFurnizor = expeditieRow.createCell(25, "Semnatura si stampila furnizorului");
			
		Cell<PDPage> cellExpeditie = expeditieRow.createCell(50, getExpeditieData());
		
		Cell<PDPage> cellPrimire = expeditieRow.createCell(25, "Semnatura de primire");
			
		footerTable.draw();


	// Close Stream and save pdf
		File file = new File("target/Factura1.pdf");
		System.out.println("Sample file saved at : " + file.getAbsolutePath());
		Files.createParentDirs(file);
		doc.save(file);
		doc.close();

		}
	
	
		private static void generateFirstRowHeaderTable(Row<PDPage> row) {
			Cell<PDPage> companyName = row.createCell(45, "<b>Furnizor:</b> Evolution Prest System SRL");
			companyName.setFontSize(9);
			companyName.setBottomPadding(2);
			
			Cell<PDPage> invoiceName = row.createCell(25, "<b>FACTURA FISCALA</b>");
			invoiceName.setFontSize(9);
			invoiceName.setAlign(HorizontalAlignment.CENTER);
			
			Cell<PDPage> clientName = row.createCell(30, "<b>Client:</b> Anton Beton");
			clientName.setFontSize(9);
		}
	
		private static void generateSecondRowHeaderTable(Row<PDPage> row) {
			Cell<PDPage> companyData = row.createCell(32, getCompanyData());
			companyData.setFontSize(7);
			companyData.setLineSpacing(1.1f);
			
			File imageFile;
			try {
				imageFile = new File(Factura1.class.getResource("/" + "imag.jpg").toURI());
				System.out.println(imageFile.getAbsolutePath());
				Cell<PDPage> logo = row.createImageCell(13, ImageUtils.readImage(imageFile));
			} catch (final URISyntaxException | IOException e) {
				e.printStackTrace();
			}
			
			Cell<PDPage> invoiceData = row.createCell(25, getInvoiceData());
			invoiceData.setFontSize(7);
			invoiceData.setLineSpacing(1.1f);
			invoiceData.setAlign(HorizontalAlignment.CENTER);
			
			Cell<PDPage> clientData = row.createCell(30, getClientData());
			clientData.setFontSize(7);
			clientData.setLineSpacing(1.1f);
		}
			
		private static String getClientData() {
			String clientData = "";
			clientData += "<b>Reg. com.:</b> J40/10329/2010<br>";
			clientData += "<b>CIF:</b> RO27633314<br>";
			clientData += "<b>Adresa:</b> Str. Ispravnicului 4 Bucuresti Sect 2 Bucuresti <br>";
			clientData += "<b>Judet:</b> Bucuresti<br>";
			clientData += "<b>Cont:</b> ROING00959300049392<br>";
			clientData += "<b>Banca:</b> ING BANK Giurgiu";
			return clientData;
		}
	
		private static String getInvoiceData() {
			String invoiceData = "";
			invoiceData += "<b>Seria</b> CTL <b>nr.</b> 000004<br>";
			invoiceData += "<b>Data</b> (zi/luna/an): 27/06/2017<br>";
			invoiceData += "<b>Cota TVA:</b> 19%<br>";
			invoiceData += "TVA la incasare";
			return invoiceData;
		}
	
		private static String getCompanyData() {
			String companyData = "";
			companyData += "<b>Nr.R.C.</b> J40/8413/2005<br>";
			companyData += "<b>CUI:</b> RO17563840<br>";
			companyData += "<b>Sediu social:</b> Bd 1 Decembrie1918, nr 20, bl 1, sc 1, et 7, ap 31, Bucuresti, Sector3<br>";
			companyData += "<b>Punct de lucru:</b> Splaiul Unirii, nr 257-259, Corp C1-C2, parter, Sector 3, Bucuresti<br>";
			companyData += "<b>Cont:</b> RO0000000000000000<br>";
			companyData += "<b>Banca:</b> ING BANK Unirii<br>";
			companyData += "<b>Cota TVA:</b> 19%, <b>Capital social:</b> 56,000.00 lei";
			return companyData;
		}
		
		private static void generateHeaderRowProductsTabel(Row<PDPage> row) {
			Cell<PDPage> cellNrCrt = row.createCell(4, "Nr. crt");
			setStyle(cellNrCrt);
	
			Cell<PDPage> cellProduct = row.createCell(51, "Denumirea produselor");
			cellProduct.setTopPadding(5);
			setStyle(cellProduct);
			
			Cell<PDPage> cellUM = row.createCell(6, "U. M.");
			cellUM.setTopPadding(5);
			setStyle(cellUM);
			
			Cell<PDPage> cellCant = row.createCell(5, "Cant.");
			cellCant.setTopPadding(5);
			setStyle(cellCant);
			
			Cell<PDPage> cellTVA = row.createCell(4, "TVA");
			cellTVA.setTopPadding(5);
			setStyle(cellTVA);
			
			Cell<PDPage> cellPretUnitar = row.createCell(10, "Pret unitar");
			cellPretUnitar.setTopPadding(5);
			setStyle(cellPretUnitar);
	
			Cell<PDPage> cellValoare = row.createCell(10, "Valoare");
			cellValoare.setTopPadding(5);
			setStyle(cellValoare);
			
			Cell<PDPage> cellValoareTVA = row.createCell(10, "Valoare TVA");
			setStyle(cellValoareTVA);
			cellValoareTVA.setTopPadding(5);
		}
	
		private static void generateNumberRow(Row<PDPage> row, String[] values) {
			Cell<PDPage> cellNrCrt = row.createCell(4, values[0]);
			setStyleRow(cellNrCrt);
			cellNrCrt.setTopPadding(4);
			
			Cell<PDPage> cellProduct = row.createCell(51, values[1]);
			setStyleRow(cellProduct);
			cellProduct.setTopPadding(4);
			
			Cell<PDPage> cellUM = row.createCell(6, values[2]);
			setStyleRow(cellUM);
			cellUM.setTopPadding(4);
			
			Cell<PDPage> cellCant = row.createCell(5, values[3]);
			setStyleRow(cellCant);
			cellCant.setTopPadding(4);
			
			Cell<PDPage> cellTVA = row.createCell(4, values[4]);
			setStyleRow(cellTVA);
			cellTVA.setTopPadding(4);
			
			Cell<PDPage> cellPretUnitar = row.createCell(10, values[5]);
			setStyleRow(cellPretUnitar);
			cellPretUnitar.setTopPadding(4);
	
			Cell<PDPage> cellValoare = row.createCell(10, values[6]);
			setStyleRow(cellValoare);
			cellValoare.setTopPadding(4);
			
			Cell<PDPage> cellValoareTVA = row.createCell(10, values[7]);
			setStyleRow(cellValoareTVA);
			cellValoareTVA.setTopPadding(4);
	}
	

		private static void generateProductRow(Row<PDPage> row, String[] values) {
			Cell<PDPage> cellNrCrt = row.createCell(4, values[0]);
			setStyleRow(cellNrCrt);
			cellNrCrt.setTopPadding(4);
			
			Cell<PDPage> cellProduct = row.createCell(51, values[1]);
			setStyleRow(cellProduct);
			cellProduct.setAlign(HorizontalAlignment.LEFT);
			cellProduct.setTopPadding(4);
			
			Cell<PDPage> cellUM = row.createCell(6, values[2]);
			setStyleRow(cellUM);
			cellUM.setTopPadding(4);
			
			Cell<PDPage> cellCant = row.createCell(5, values[3]);
			setStyleRow(cellCant);
			cellCant.setTopPadding(4);
			
			Cell<PDPage> cellTVA = row.createCell(4, values[4]);
			setStyleRow(cellTVA);
			cellTVA.setTopPadding(4);
			
			Cell<PDPage> cellPretUnitar = row.createCell(10, values[5]);
			setStyleRow(cellPretUnitar);
			cellPretUnitar.setAlign(HorizontalAlignment.RIGHT);
			cellPretUnitar.setTopPadding(4);

			Cell<PDPage> cellValoare = row.createCell(10, values[6]);
			setStyleRow(cellValoare);
			cellValoare.setAlign(HorizontalAlignment.RIGHT);
			cellValoare.setTopPadding(4);
			
			Cell<PDPage> cellValoareTVA = row.createCell(10, values[7]);
			setStyleRow(cellValoareTVA);
			cellValoareTVA.setAlign(HorizontalAlignment.RIGHT);
			cellValoareTVA.setTopPadding(5);
	}

		
		private static void generateEmptyRow(Row<PDPage> row, String[] values) {
			Cell<PDPage> cellNrCrt = row.createCell(4, values[0]);
			cellNrCrt.setBottomBorderStyle(new LineStyle(Color.WHITE, 0));
			
			Cell<PDPage> cellProduct = row.createCell(51, values[1]);
			cellProduct.setBottomBorderStyle(new LineStyle(Color.WHITE, 0));
			
			Cell<PDPage> cellUM = row.createCell(6, values[2]);
			cellUM.setBottomBorderStyle(new LineStyle(Color.WHITE, 0));
			
			Cell<PDPage> cellCant = row.createCell(5, values[3]);
			cellCant.setBottomBorderStyle(new LineStyle(Color.WHITE, 0));
			
			Cell<PDPage> cellTVA = row.createCell(4, values[4]);
			cellTVA.setBottomBorderStyle(new LineStyle(Color.WHITE, 0));
			
			Cell<PDPage> cellPretUnitar = row.createCell(10, values[5]);
			cellPretUnitar.setBottomBorderStyle(new LineStyle(Color.WHITE, 0));

			Cell<PDPage> cellValoare = row.createCell(10, values[6]);
			cellValoare.setBottomBorderStyle(new LineStyle(Color.WHITE, 0));
			
			Cell<PDPage> cellValoareTVA = row.createCell(10, values[7]);
			cellValoareTVA.setBottomBorderStyle(new LineStyle(Color.WHITE, 0));
	}
		
		
		private static void setStyle(Cell<PDPage> cell) {
			cell.setFont(PDType1Font.HELVETICA_BOLD);
			cell.setFontSize(7);
			cell.setFillColor(Color.GRAY);
			cell.setTextColor(Color.WHITE);
			cell.setAlign(HorizontalAlignment.CENTER);
		}

		private static void setStyleRow(Cell<PDPage> cell) {
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(6);
			cell.setAlign(HorizontalAlignment.CENTER);
		}
		
		private static String getExpeditieData() {
			String expeditieData = "";
			expeditieData += "Date privind expeditia<br>";
			expeditieData += "<b>Delegat: </b> Gogu Martalogu<br>";
			expeditieData += "<b>BI/CI seria:</b> TT <b>nr.:</b> 4598756<br>";
			expeditieData += "<b>CNP:</b> 1958745896545<br>";
			expeditieData += "<b>Mijloc de transport:</b> Racheta intergalactica<br>";
			expeditieData += "<b>Nr.:</b> B 01 MAI<br>";
			expeditieData += "<b>Expeditia s-a efectuat la data si ora:</b> 06/11/2017 15:22";
			return expeditieData;
		}
		
		private static PDPage addNewPage(PDDocument doc) {
			PDPage page = new PDPage();
			doc.addPage(page);
			return page;
		}

}
